import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ProblemList from '@/views/ProblemList.vue'
import NoticeDetail from '@/views/NoticeDetail.vue'
import ChoiceProblemList from '@/views/ChoiceProblemList.vue'
import FillInBlanksProblemList from '@/views/FillInBlanksProblemList.vue'
import ProblemDetail from '@/views/ProblemDetail.vue'
import ChoiceProblemDetail from '@/views/ChoiceProblemDetail.vue'
import FillInBlanksProblemDetail from '@/views/FillInBlanksProblemDetail.vue'
import Management from '@/views/Management.vue'
import EditorCodeProblem from '@/views/EditorCodeProblem.vue'
import Login from '@/views/Login.vue'
import Register from '@/views/Register.vue'
import Group from '@/views/Group.vue'
import EditorChoiceProblem from "@/views/EditorChoiceProblem";
import EditorFillProblem from "@/views/EditorFillProblem";
import SendTask from '@/views/SendTask';
import echarts from 'echarts'

Vue.use(VueRouter)


const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/problem',
    name: 'ProblemList',
    component: ProblemList
  },
  {
    path: '/notice/detail',
    name: 'noticeDetail',
    component: NoticeDetail
  },
  {
    path: '/problem/choice',
    name: 'choiceProblemList',
    component: ChoiceProblemList
  },
  {
    path: '/problem/fillinblanks',
    name: 'fillInBlanksProblemList',
    component: FillInBlanksProblemList
  },
  {
    path: '/problem/problem/details',
    name: 'ProblemDetail',
    component: ProblemDetail
  },
  {
    path: '/problem/choice/details',
    name: 'ChoiceProblemDetail',
    component: ChoiceProblemDetail
  },
  {
    path: '/problem/fillinblanks/details',
    name: 'FillInBlanksProblemDetail',
    component: FillInBlanksProblemDetail
  },
  {
    path: '/management',
    name: 'Management',
    component: Management
  },
  {
    path: '/management/editorCodeProblem',
    name: 'EditorCodeProblem',
    component: EditorCodeProblem
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/group',
    name: 'Group',
    component: Group
  },
  {
    path: '/management/editorChoiceProblem',
    name: 'EditorChoiceProblem',
    component: EditorChoiceProblem
  },
  {
    path: '/management/editorFillProblem',
    name: 'EditorFillProblem',
    component: EditorFillProblem
  },
  {
    path: '/management/sendTask',
    name: 'SendTask',
    component: SendTask
  }
]

const router = new VueRouter({
  mode: "hash",
  routes
})


export default router
