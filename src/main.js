import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import qs from 'qs'

//导入cookies工具
import VueCookie from "vue-cookies";
Vue.use(VueCookie);

//导入echarts
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts
Vue.prototype.qs = qs

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
